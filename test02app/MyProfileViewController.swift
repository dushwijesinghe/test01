//
//  MyProfileViewController.swift
//  test02app
//
//  Created by Digital-03 on 2/28/19.
//  Copyright © 2019 Digital-03. All rights reserved.
//

import UIKit

class MyProfileViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func myProfileClick(_ sender: Any) {
        self.performSegue(withIdentifier: "goToProfileImage", sender: self)
    }
    
    
    @IBAction func backOnClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
