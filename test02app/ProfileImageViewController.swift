//
//  ProfileImageViewController.swift
//  test02app
//
//  Created by Digital-03 on 2/28/19.
//  Copyright © 2019 Digital-03. All rights reserved.
//

import UIKit

class ProfileImageViewController: UIViewController {

    
    @IBOutlet weak var profileImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.profileImage.image = UIImage(named: "logo")
        
        self.profileImage.layer.cornerRadius = 100

        // Do any additional setup after loading the view.
    }
    

    @IBAction func backButtonClick(_ sender: Any) {
        self.navigationController?.popToViewController(self, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
